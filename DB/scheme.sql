DROP TABLE IF EXISTS cinemas_table CASCADE;
DROP TABLE IF EXISTS price_tariffs CASCADE;
DROP TABLE IF EXISTS screen_type CASCADE;
DROP TABLE IF EXISTS halls_table CASCADE;
DROP TABLE IF EXISTS seats_table CASCADE;
DROP TABLE IF EXISTS seances_table CASCADE;
DROP TABLE IF EXISTS customers_table CASCADE;
DROP TABLE IF EXISTS movies_table CASCADE;
DROP TABLE IF EXISTS movie_genres CASCADE;
DROP TABLE IF EXISTS movie_showing_table CASCADE;
DROP TABLE IF EXISTS all_available_seats CASCADE;
DROP TABLE IF EXISTS booking_table CASCADE;
DROP TABLE IF EXISTS movies_logging CASCADE;
DROP TYPE IF EXISTS seat_status CASCADE;

CREATE TABLE cinemas_table
(
  cinema_id serial,
  manager_name character varying NOT NULL,
  manager_phone character(10) NOT NULL UNIQUE,
  cinema_name character varying NOT NULL UNIQUE,
  cinema_adress character varying UNIQUE,
  hall_number integer NOT NULL DEFAULT 1 CHECK (hall_number > 0 ),
  support_3d boolean DEFAULT False,
  cafe_type varchar DEFAULT 'Not available',
  wi_fi boolean DEFAULT False,
  parking_space integer DEFAULT 0,
  PRIMARY KEY (cinema_id)
);

CREATE TABLE screen_type
(
  screen_type_id serial,
  screen_width integer NOT NULL,
  screen_height integer NOT NULL,
  PRIMARY KEY (screen_type_id),
  CHECK (screen_width BETWEEN 2 and 30),
  CHECK (screen_height BETWEEN 1 and 20)
);

CREATE TABLE halls_table
(
  hall_id serial,
  hall_name character varying,
  cinema_id integer NOT NULL,
  screen integer NOT NULL,
  support_3d boolean DEFAULT False,
  PRIMARY KEY (hall_id),
  FOREIGN KEY (cinema_id)
      REFERENCES cinemas_table (cinema_id),
  FOREIGN KEY (screen)
      REFERENCES screen_type (screen_type_id),
  UNIQUE (hall_name,cinema_id)

);


CREATE TABLE price_tariffs
(
  price_id serial,
  tariff_name character varying NOT NULL,
  workday boolean DEFAULT True NOT NULL,
  weekend boolean DEFAULT True NOT NULL,
  base_price numeric DEFAULT 30,
  PRIMARY KEY (price_id)
);


CREATE TABLE seats_table
(
  hall_id integer NOT NULL,
  row integer NOT NULL,
  seat integer NOT NULL,
  PRIMARY KEY (hall_id, row, seat),
  FOREIGN KEY (hall_id)
      REFERENCES halls_table (hall_id)
      ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE movie_genres
(
  movie_genre_id serial,
  genre_name character varying NOT NULL UNIQUE,
  PRIMARY KEY (movie_genre_id)
);


CREATE TABLE movies_table
(
  movie_id serial,
  movie_name character varying NOT NULL UNIQUE,
  movie_duration interval,
  movie_genre integer NOT NULL DEFAULT 2,
  movie_rating numeric NOT NULL DEFAULT 1,
  PRIMARY KEY (movie_id),
  FOREIGN KEY (movie_genre)
      REFERENCES movie_genres (movie_genre_id)
);

CREATE TABLE movie_showing_table
(
  movie_showing_id serial,
  hall_id integer NOT NULL,
  movie_id integer NOT NULL,
  showing_dates date NOT NULL DEFAULT CURRENT_DATE,
  days_showing integer NOT NULL DEFAULT 7,
  PRIMARY KEY (movie_showing_id),
  FOREIGN KEY (movie_id)
      REFERENCES movies_table (movie_id),
  FOREIGN KEY (hall_id)
      REFERENCES halls_table (hall_id) 

);

CREATE TABLE seances_table
(
  seance_id serial,
  hall_id integer,
  perfomanse_start_time time without time zone,
  price integer,
  PRIMARY KEY (seance_id),
  FOREIGN KEY (hall_id)
	  REFERENCES halls_table (hall_id),
  FOREIGN KEY (price)
	  REFERENCES price_tariffs (price_id)
);

CREATE TABLE customers_table
(
  customer_id serial,
  customer_name character varying NOT NULL,
  phone_number character varying(10),
  PRIMARY KEY (customer_id)
);

CREATE TABLE booking_table
(
  booking_id serial,
  booking_for_date date NOT NULL,
  booking_made_date date NOT NULL DEFAULT ('now'::text)::date,
  booking_seat_count integer NOT NULL,
  customer integer,
  PRIMARY KEY (booking_id),
  FOREIGN KEY (customer)
      REFERENCES customers_table (customer_id) 
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TYPE seat_status AS ENUM ('available', 'booked', 'soled');

CREATE TABLE all_available_seats
(
  hall_id integer NOT NULL,
  row integer NOT NULL,
  seat integer NOT NULL,
  perfomance_date date NOT NULL,
  seat_status_code seat_status,
  seance_id integer NOT NULL,
  booking_id integer,
  movie_showing integer NOT NULL,
  price_id integer NOT NULL,
  PRIMARY KEY (perfomance_date, seance_id, hall_id, row, seat),
  FOREIGN KEY (booking_id)
      REFERENCES booking_table (booking_id),
  FOREIGN KEY (movie_showing)
      REFERENCES movie_showing_table (movie_showing_id),
  FOREIGN KEY (price_id)
      REFERENCES price_tariffs (price_id),
  FOREIGN KEY (seance_id)
      REFERENCES seances_table (seance_id),
  FOREIGN KEY (hall_id, row, seat)
      REFERENCES seats_table (hall_id, row, seat)
);

------------------------------�������--------------------------
--�������, ������� ������������ ��������� ���������� ��� ���������� ������� � �������

--���������� �������
CREATE OR REPLACE FUNCTION tr_func_for_insertion () RETURNS trigger AS $tr_func$ 
DECLARE
BEGIN 
--�������� ������� �����������
  IF NEW.movie_showing_id IS NULL THEN
            RAISE EXCEPTION 'movie_showing_id cannot be null';
        END IF;
  IF NEW.showing_dates IS NULL THEN
            RAISE EXCEPTION 'showing_dates cannot be null';
        END IF;
--�������� ������������� ��������������� ��������� � ������� ������
  IF (SELECT movie_id FROM movies_table WHERE movie_id = NEW.movie_id) IS NULL THEN
			RAISE EXCEPTION 'this foreign key movie_id does not exist';
        END IF;
  IF (SELECT hall_id FROM halls_table WHERE hall_id = NEW.hall_id) IS NULL THEN
			RAISE EXCEPTION 'this foreign key hall_id does not exist';
        END IF;  
  RETURN NEW;
END; 
$tr_func$ LANGUAGE  plpgsql;

--�������, ���������� �������
CREATE TRIGGER movie_showing_check_trig BEFORE INSERT OR UPDATE
        ON movie_showing_table FOR each ROW
        EXECUTE PROCEDURE tr_func_for_insertion ();
		
		

--�������, ������� ����� ����������� ������� ������� ��� ���������� ���������� �������� �������		
CREATE OR REPLACE FUNCTION tr_func_calc_rating () RETURNS trigger AS $tr_calc_rating$ 
	DECLARE
        number_of_tickets INTEGER;
		start_date DATE;
		number_of_days INTEGER;
        rating DECIMAL (6,2);
		m_id INTEGER;
		m_sh_id INTEGER;
		
    BEGIN
	--�������� ��������� ���� ������ �������
        SELECT INTO start_date showing_dates FROM movie_showing_table WHERE movie_showing_id = NEW.movie_showing;
	--������� ��������� ���� ������ �� ����������� ����
		number_of_days = CURRENT_DATE - start_date;
	--�������� ���������� �������� �������
		SELECT INTO number_of_tickets COUNT(*) FROM all_available_seats WHERE movie_showing = NEW.movie_showing AND seat_status_code = 'soled'; 
    --������� �������    
		rating = ROUND(CAST(number_of_tickets AS DECIMAL)/number_of_days,2);
	--�������� ����� � ������ id
        SELECT INTO m_id  movie_id FROM movie_showing_table WHERE movie_showing_id = NEW.movie_showing ;
		UPDATE movies_table SET movie_rating = rating WHERE movie_id = m_id;       
        RETURN NEW;
    END; 
$tr_calc_rating$ LANGUAGE  plpgsql;

CREATE TRIGGER calc_movie_rating AFTER INSERT OR UPDATE
        ON all_available_seats FOR each ROW
        EXECUTE PROCEDURE tr_func_calc_rating ();


--������� ��� ����������� ��������� ��� ������� �������

--�������� ������� ��� �����������
CREATE TABLE movies_logging
(
  user_name VARCHAR NOT NULL,
  date timestamptz NOT NULL,
  event VARCHAR NOT NULL,
  movie_id INTEGER NOT NULL,
  movie_name character varying NOT NULL,
  movie_duration time without time zone,
  movie_genre integer NOT NULL,
  movie_rating numeric NOT NULL
);
		
CREATE OR REPLACE FUNCTION movies_log() RETURNS TRIGGER AS $function$
        BEGIN
                IF (TG_OP = 'DELETE') THEN
                   INSERT INTO movies_logging SELECT user, now(), TG_OP, OLD.movie_id, OLD.movie_name, 
                          OLD.movie_duration, OLD.movie_genre, OLD.movie_rating;
                   RETURN OLD;
                ELSIF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
                   INSERT INTO movies_logging SELECT user, now(), TG_OP, NEW.movie_id, NEW.movie_name, 
                          NEW.movie_duration, NEW.movie_genre, NEW.movie_rating;
                   RETURN NEW;
                END IF;
        END;
$function$ LANGUAGE plpgsql;

CREATE TRIGGER movies_log
AFTER INSERT OR UPDATE OR DELETE ON movies_table
    FOR EACH ROW EXECUTE PROCEDURE movies_log();
	
	
	
---------------------------------������������� ----------------------------
--c������� ������������� ��� ����������� ������ movies_table � movie_showing_table, ������� ����� �������� ������,
--������� ������������� �������, ������� ���� � ������� - ��� ����� �������������� ������ �� �������.
--��� ��� ��� ������������� ���� ������, ��� ������������ �� ��� ������ �� ���� ������ - ������������� ������� �� ��������
CREATE VIEW movies_vw 
	AS SELECT mst.movie_id, mt.movie_name, mt.movie_duration, mt.movie_rating, mst.hall_id, mst.movie_showing_id 
		FROM movies_table mt LEFT JOIN movie_showing_table mst ON (mt.movie_id = mst.movie_id)
			WHERE mst.showing_dates + mst.days_showing >= CURRENT_DATE;

GRANT SELECT ON movies_vw TO client, guest;
REVOKE ALL PRIVILEGES ON movies_table, movie_showing_table FROM client, guest;
			
--������� ������������� ����������� � ������� RULES
--INSERT RULE
CREATE RULE movies_vw_INSERT AS ON INSERT TO movies_vw DO INSTEAD (
    INSERT INTO  movie_showing_table(movie_id, hall_id) VALUES (NEW.movie_id,NEW.hall_id);
    INSERT INTO  movies_table(movie_name, movie_duration,movie_rating) 
		VALUES (NEW.movie_name, NEW.movie_duration, NEW.movie_rating);
        );

--UPDATE RULE
CREATE RULE movies_vw_UPDATE AS ON UPDATE TO movies_vw DO INSTEAD (
        UPDATE movie_showing_table
			SET movie_id = NEW.movie_id, hall_id = NEW.hall_id 
				WHERE movie_showing_id = OLD.movie_showing_id;
        UPDATE movies_table
			SET movie_name = NEW.movie_name,movie_duration = NEW.movie_duration, movie_rating = NEW.movie_rating 
				WHERE movie_id = OLD.movie_id;
       );
	 
--DELETE RULE
CREATE OR REPLACE RULE movies_vw_DELETE AS ON DELETE TO movies_vw DO INSTEAD (
    DELETE FROM movies_table WHERE movie_id = OLD.movie_id;
    DELETE FROM movie_showing_table WHERE movie_id = OLD.movie_id;
    );


--������� ������������� ����������� � ������� INSTEAD OF Trigger
CREATE OR REPLACE FUNCTION movies_vw_tr_func()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
   BEGIN
      IF TG_OP = 'INSERT' THEN
        INSERT INTO  movie_showing_table(movie_id, hall_id) VALUES(NEW.movie_id,NEW.hall_id);
        INSERT INTO  movies_table(movie_name, movie_duration,movie_rating) 
			VALUES(NEW.movie_name, NEW.movie_duration, NEW.movie_rating);
        RETURN NEW;
      ELSIF TG_OP = 'UPDATE' THEN
        UPDATE movie_showing_table
			SET movie_id = NEW.movie_id, hall_id = NEW.hall_id 
				WHERE movie_showing_id = OLD.movie_showing_id;
        UPDATE movies_table
			SET movie_name = NEW.movie_name,movie_duration = NEW.movie_duration, movie_rating = NEW.movie_rating 
				WHERE movie_id = OLD.movie_id;
		RETURN NEW;
      ELSIF TG_OP = 'DELETE' THEN
		DELETE FROM movies_table WHERE movie_id = OLD.movie_id;
		DELETE FROM movie_showing_table WHERE movie_id = OLD.movie_id;
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$function$;

CREATE TRIGGER movies_vw_tr
    INSTEAD OF INSERT OR UPDATE OR DELETE ON
      movies_vw FOR EACH ROW EXECUTE PROCEDURE movies_vw_tr_func();

