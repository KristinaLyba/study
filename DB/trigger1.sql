--�������, ������� ������������ ��������� ���������� ��� ���������� ������� � �������

--���������� �������
DROP TRIGGER movie_showing_check_trig ON movie_showing_table CASCADE;
CREATE OR REPLACE FUNCTION tr_func_for_insertion () RETURNS trigger AS $tr_func$ 
DECLARE
BEGIN 
--�������� ������� �����������
  IF NEW.movie_showing_id IS NULL THEN
            RAISE EXCEPTION 'movie_showing_id cannot be null';
        END IF;
  IF NEW.showing_dates IS NULL THEN
            RAISE EXCEPTION 'showing_dates cannot be null';
        END IF;
--�������� ������������� ��������������� ��������� � ������� ������
  IF (SELECT movie_id FROM movies_table WHERE movie_id = NEW.movie_id) IS NULL THEN
			RAISE EXCEPTION 'this foreign key movie_id does not exist';
        END IF;
  IF (SELECT hall_id FROM halls_table WHERE hall_id = NEW.hall_id) IS NULL THEN
			RAISE EXCEPTION 'this foreign key hall_id does not exist';
        END IF;  
  RETURN NEW;
END; 
$tr_func$ LANGUAGE  plpgsql;

--�������, ���������� �������
CREATE TRIGGER movie_showing_check_trig BEFORE INSERT OR UPDATE
        ON movie_showing_table FOR each ROW
        EXECUTE PROCEDURE tr_func_for_insertion ();
		
		
DROP TRIGGER calc_movie_rating ON all_available_seats CASCADE;


--�������, ������� ����� ����������� ������� ������� ��� ���������� ���������� �������� �������		
CREATE OR REPLACE FUNCTION tr_func_calc_rating () RETURNS trigger AS $tr_calc_rating$ 
	DECLARE
        number_of_tickets INTEGER;
		start_date DATE;
		number_of_days INTEGER;
        rating DECIMAL (6,2);
		m_id INTEGER;
		m_sh_id INTEGER;
		
    BEGIN
	--�������� ��������� ���� ������ �������
        SELECT INTO start_date showing_dates FROM movie_showing_table WHERE movie_showing_id = NEW.movie_showing;
	--������� ��������� ���� ������ �� ����������� ����
		number_of_days = CURRENT_DATE - start_date;
	--�������� ���������� �������� �������
		SELECT INTO number_of_tickets COUNT(*) FROM all_available_seats WHERE movie_showing = NEW.movie_showing AND seat_status_code = 3; 
    --������� �������    
		rating = ROUND(CAST(number_of_tickets AS DECIMAL)/number_of_days,2);
	--�������� ����� � ������ id
        SELECT INTO m_id  movie_id FROM movie_showing_table WHERE movie_showing_id = NEW.movie_showing ;
		UPDATE movies_table SET movie_rating = rating WHERE movie_id = m_id;       
        RETURN NEW;
    END; 
$tr_calc_rating$ LANGUAGE  plpgsql;
CREATE TRIGGER calc_movie_rating AFTER INSERT OR UPDATE
        ON all_available_seats FOR each ROW
        EXECUTE PROCEDURE tr_func_calc_rating ();


--������� ��� ����������� ��������� ��� ������� �������

--�������� ������� ��� �����������
CREATE TABLE movies_logging
(
  user_name VARCHAR NOT NULL,
  date timestamptz NOT NULL,
  event VARCHAR NOT NULL,
  movie_id INTEGER NOT NULL,
  movie_name character varying NOT NULL,
  movie_duration time without time zone,
  movie_genre integer NOT NULL,
  movie_rating numeric NOT NULL
);
		
CREATE OR REPLACE FUNCTION movies_log() RETURNS TRIGGER AS $function$
        BEGIN
                IF (TG_OP = 'DELETE') THEN
                   INSERT INTO movies_logging SELECT user, now(), TG_OP, OLD.movie_id, OLD.movie_name, 
                          OLD.movie_duration, OLD.movie_genre, OLD.movie_rating;
                   RETURN OLD;
                ELSIF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
                   INSERT INTO movies_logging SELECT user, now(), TG_OP, NEW.movie_id, NEW.movie_name, 
                          NEW.movie_duration, NEW.movie_genre, NEW.movie_rating;
                   RETURN NEW;
                END IF;
        END;
$function$ LANGUAGE plpgsql;

CREATE TRIGGER movies_log
AFTER INSERT OR UPDATE OR DELETE ON movies_table
    FOR EACH ROW EXECUTE PROCEDURE movies_log();