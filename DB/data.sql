ALTER SEQUENCE cinemas_table_cinema_id_seq RESTART WITH 1;
INSERT INTO cinemas_table VALUES(DEFAULT,'Jeff','0991234567','Spaceship','Golf side lane,12,3',2);
INSERT INTO cinemas_table VALUES(DEFAULT,'Marina','0991234267','Garlim Shake','Strawberry street, 666',1);


ALTER SEQUENCE screen_type_screen_type_id_seq RESTART WITH 1;
INSERT INTO screen_type VALUES(DEFAULT,25,10);
INSERT INTO screen_type VALUES(DEFAULT,28,15);
INSERT INTO screen_type VALUES(DEFAULT,20,8);
INSERT INTO screen_type VALUES(DEFAULT,13,7);

ALTER SEQUENCE halls_table_hall_id_seq RESTART WITH 1;
INSERT INTO halls_table VALUES(DEFAULT,'First',1,1);
INSERT INTO halls_table VALUES(DEFAULT,'Second',1,2,True);
INSERT INTO halls_table VALUES(DEFAULT,'Main',2,1);

ALTER SEQUENCE movie_genres_movie_genre_id_seq RESTART WITH 1;
INSERT INTO movie_genres VALUES(DEFAULT,'Drama');
INSERT INTO movie_genres VALUES(DEFAULT,'Comedy');
INSERT INTO movie_genres VALUES(DEFAULT,'Thriller');
INSERT INTO movie_genres VALUES(DEFAULT,'Scary');
INSERT INTO movie_genres VALUES(DEFAULT,'Melodrama');
INSERT INTO movie_genres VALUES(DEFAULT,'For Kids');

ALTER SEQUENCE price_tariffs_price_id_seq RESTART WITH 1;
INSERT INTO price_tariffs VALUES(DEFAULT,'ChildTime10-12');
INSERT INTO price_tariffs VALUES(DEFAULT,'ChildTime12-16');
INSERT INTO price_tariffs VALUES(DEFAULT,'ChildTime16-21');
INSERT INTO price_tariffs VALUES(DEFAULT,'StudentTime10-12');
INSERT INTO price_tariffs VALUES(DEFAULT,'StudentTime12-18');
INSERT INTO price_tariffs VALUES(DEFAULT,'UsualTime10-12');
INSERT INTO price_tariffs VALUES(DEFAULT,'UsualTime12-16');
INSERT INTO price_tariffs VALUES(DEFAULT,'UsualTime16-21');
INSERT INTO price_tariffs VALUES(DEFAULT,'UsualTime21-24');
INSERT INTO price_tariffs VALUES(DEFAULT,'WorkdayAnyTime');
INSERT INTO price_tariffs VALUES(DEFAULT,'WeekendAnyTime');


ALTER SEQUENCE seances_table_seance_id_seq RESTART WITH 1;
INSERT INTO seances_table VALUES(DEFAULT,1,'10:00',1);
INSERT INTO seances_table VALUES(DEFAULT,2,'10:30',1);
INSERT INTO seances_table VALUES(DEFAULT,1,'13:45',5);
INSERT INTO seances_table VALUES(DEFAULT,2,'15:30',5);
INSERT INTO seances_table VALUES(DEFAULT,1,'16:10',10);
INSERT INTO seances_table VALUES(DEFAULT,2,'18:00',10);
INSERT INTO seances_table VALUES(DEFAULT,2,'20:00',10);
INSERT INTO seances_table VALUES(DEFAULT,1,'22:30',9);
INSERT INTO seances_table VALUES(DEFAULT,2,'23:00',9);

ALTER SEQUENCE movies_table_movie_id_seq RESTART WITH 1;
INSERT INTO movies_table VALUES(DEFAULT,'Matrix','00:01:54',3,5);
INSERT INTO movies_table VALUES(DEFAULT,'Christmas','00:02:05',6,5);
INSERT INTO movies_table VALUES(DEFAULT,'In time','00:01:40',2,5);
INSERT INTO movies_table VALUES(DEFAULT,'Titanic','00:02:54',1,5);
INSERT INTO movies_table VALUES(DEFAULT,'Astral','00:01:50',4,5);
INSERT INTO movies_table VALUES(DEFAULT,'The Twilight','00:01:54',5,5);

ALTER SEQUENCE movie_showing_table_movie_showing_id_seq RESTART WITH 1;
INSERT INTO movie_showing_table VALUES(DEFAULT,1,1,'2013-03-25', 8);
INSERT INTO movie_showing_table VALUES(DEFAULT,2,6,'2013-03-25', 10);
INSERT INTO movie_showing_table VALUES(DEFAULT,3,1,'2013-02-05', 12);
INSERT INTO movie_showing_table VALUES(DEFAULT,3,3,'2013-03-01',8);
INSERT INTO movie_showing_table VALUES(DEFAULT,2,6,'2013-03-27', 25);
INSERT INTO movie_showing_table VALUES(DEFAULT,1,4,'2013-03-10', 15);

INSERT INTO seats_table VALUES(1,1,1);
INSERT INTO seats_table VALUES(1,1,2);
INSERT INTO seats_table VALUES(1,1,3);
INSERT INTO seats_table VALUES(1,2,1);
INSERT INTO seats_table VALUES(1,2,2);
INSERT INTO seats_table VALUES(1,2,3);
INSERT INTO seats_table VALUES(2,1,1);
INSERT INTO seats_table VALUES(2,1,2);
INSERT INTO seats_table VALUES(2,1,3);
INSERT INTO seats_table VALUES(3,1,1);
INSERT INTO seats_table VALUES(3,1,2);
INSERT INTO seats_table VALUES(3,2,1);
INSERT INTO seats_table VALUES(3,2,2);
INSERT INTO seats_table VALUES(3,3,1);
INSERT INTO seats_table VALUES(3,3,2);

ALTER SEQUENCE customers_table_customer_id_seq RESTART WITH 1;
INSERT INTO customers_table VALUES(DEFAULT,'Galina','12345');
INSERT INTO customers_table VALUES(DEFAULT,'Maria','22345');
INSERT INTO customers_table VALUES(DEFAULT,'Artem','32345');
INSERT INTO customers_table VALUES(DEFAULT,'Mishel','42345');
INSERT INTO customers_table VALUES(DEFAULT,'Elena','512345');
INSERT INTO customers_table VALUES(DEFAULT,'Aleksey','62345');

ALTER SEQUENCE booking_table_booking_id_seq RESTART WITH 1;
INSERT INTO booking_table VALUES(DEFAULT,'2013-04-01','2013-03-26',2,1);
INSERT INTO booking_table VALUES(DEFAULT,'2013-04-02','2013-03-25',4,2);
INSERT INTO booking_table VALUES(DEFAULT,'2013-04-02','2013-03-27',1,3);
INSERT INTO booking_table VALUES(DEFAULT,'2013-04-03','2013-03-27',1,4);
INSERT INTO booking_table VALUES(DEFAULT,'2013-04-03','2013-03-27',5,5);
INSERT INTO booking_table VALUES(DEFAULT,'2013-03-28','2013-03-21',4,6);

INSERT INTO all_available_seats VALUES(1,1,1,'2013-04-01','booked',1,1,3,10);
INSERT INTO all_available_seats VALUES(1,1,2,'2013-04-01','available',1,NULL,3,10);
INSERT INTO all_available_seats VALUES(1,1,3,'2013-04-01','soled',1,NULL,3,10);
INSERT INTO all_available_seats VALUES(1,2,1,'2013-04-01','soled',1,2,3,10);
INSERT INTO all_available_seats VALUES(1,2,2,'2013-04-01','available',1,NULL,3,10);
INSERT INTO all_available_seats VALUES(1,2,3,'2013-04-01','available',1,NULL,3,10);