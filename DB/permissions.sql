CREATE ROLE dba WITH SUPERUSER CREATEROLE CREATEDB LOGIN;
CREATE ROLE administrator WITH CREATEROLE LOGIN;
CREATE ROLE client WITH LOGIN;
CREATE ROLE guest WITH NOLOGIN;

GRANT CONNECT ON DATABASE cinema TO client;
GRANT CONNECT ON DATABASE cinema TO administrator;

  --dba doesn't need any grants because it is a superuser
  
  --grants for administrator
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA public TO administrator;

  --*****grants by tables for client and guest*****
  
  --grants for AllAvailableSeats
GRANT SELECT(seat_status_code, seat_id, seance_id, perfomance_date, movie_showing) ON all_available_seats TO client;

  --grants for Booking
  
  --grants for Cinemas
GRANT SELECT(cinema_name, cinema_adress, manager_phone, manager_name) ON cinemas_table TO client, guest;

  --grants for Customers
  
  --grants for Halls
GRANT SELECT(hall_name, type) ON halls_table TO client;

  --grants for HallsCatalog

  --grants for Movies

  --grants for MovieGenre

  --grants for MovieShowing
GRANT SELECT(hall_id, movie_id, showing_dates) ON movie_showing_table TO client, guest;

  --grants for Seances
GRANT SELECT(perfomance_start_time) ON seances_table TO client;

  --grants for Seats

  --grants for SeatStatus  
  