--requests for comparing
--first
SELECT * FROM movies_table 
WHERE movies_table.movie_genre = 4 or movies_table.movie_genre = 5 
and movies_table.movie_duration = '01:54:00'


--second
SELECT * FROM movie_showing_table 
WHERE movie_showing_table.movie_showing_id >= 1000 AND movie_showing_table.movie_showing_id <= 8000 AND
movie_showing_table.showing_dates = TO_DATE('27-MAR-2013', 'DD-MON-YYYY')

--start analyzing
EXPLAIN ANALYZE SELECT * FROM movies_table 
WHERE movies_table.movie_genre = 4 or movies_table.movie_genre = 5 and movies_table.movie_duration = '01:54:00'

EXPLAIN ANALYZE SELECT * FROM movie_showing_table 
WHERE movie_showing_table.movie_showing_id >= 1000 AND movie_showing_table.movie_showing_id <= 8000 AND
movie_showing_table.showing_dates = TO_DATE('27-MAR-2013', 'DD-MON-YYYY') 

--have some results


--creating indexes
DROP INDEX movies_table_genre_index ;
DROP INDEX movies_table_duration_index ;
DROP INDEX movie_showing_hall_index;
DROP INDEX movie_showing_dates_index;

CREATE INDEX movies_table_genre_index ON movies_table(movie_genre);
CREATE INDEX movies_table_duration_index ON movies_table(movie_duration);

CREATE INDEX movie_showing_hall_index ON movie_showing_table(hall_id);
CREATE INDEX movie_showing_dates_index ON movie_showing_table(showing_dates);


--analyze again
EXPLAIN ANALYZE SELECT * FROM movies_table 
WHERE movies_table.movie_genre = 4 or movies_table.movie_genre = 5 and movies_table.movie_duration = '01:54:00'

EXPLAIN ANALYZE SELECT * FROM movie_showing_table 
WHERE movie_showing_table.movie_showing_id >= 1000 AND movie_showing_table.movie_showing_id <= 8000 AND
movie_showing_table.showing_dates = TO_DATE('27-MAR-2013', 'DD-MON-YYYY') 





