--c������� ������������� ��� ����������� ������ movies_table � movie_showing_table, ������� ����� �������� ������,
--������� ������������� �������, ������� ���� � ������� - ��� ����� �������������� ������ �� �������.
--��� ��� ��� ������������� ���� ������, ��� ������������ �� ��� ������ �� ���� ������ - ������������� ������� �� ��������
SELECT * FROM movies_vw;
INSERT INTO movies_vw VALUES (1, 'NEW MOVIE', '00:00:00', 1.11, 2,DEFAULT);


CREATE VIEW movies_vw 
	AS SELECT mst.movie_id, mt.movie_name, mt.movie_duration, mt.movie_rating, mst.hall_id, mst.movie_showing_id 
		FROM movies_table mt LEFT JOIN movie_showing_table mst ON (mt.movie_id = mst.movie_id)
			WHERE mst.showing_dates + mst.days_showing >= CURRENT_DATE;

GRANT SELECT ON movies_vw TO client, guest;
REVOKE ALL PRIVILEGES ON movies_table, movie_showing_table FROM client, guest;
			
--������� ������������� ����������� � ������� RULES
--INSERT RULE
CREATE RULE movies_vw_INSERT AS ON INSERT TO movies_vw DO INSTEAD (
    INSERT INTO  movie_showing_table(movie_id, hall_id) VALUES (NEW.movie_id,NEW.hall_id);
    INSERT INTO  movies_table(movie_name, movie_duration,movie_rating) 
		VALUES (NEW.movie_name, NEW.movie_duration, NEW.movie_rating);
        );

--UPDATE RULE
CREATE RULE movies_vw_UPDATE AS ON UPDATE TO movies_vw DO INSTEAD (
        UPDATE movie_showing_table
			SET movie_id = NEW.movie_id, hall_id = NEW.hall_id 
				WHERE movie_showing_id = OLD.movie_showing_id;
        UPDATE movies_table
			SET movie_name = NEW.movie_name,movie_duration = NEW.movie_duration, movie_rating = NEW.movie_rating 
				WHERE movie_id = OLD.movie_id;
       );
	 
--DELETE RULE
CREATE OR REPLACE RULE movies_vw_DELETE AS ON DELETE TO movies_vw DO INSTEAD (
    DELETE FROM movies_table WHERE movie_id = OLD.movie_id;
    DELETE FROM movie_showing_table WHERE movie_id = OLD.movie_id;
    );


--������� ������������� ����������� � ������� INSTEAD OF Trigger
CREATE OR REPLACE FUNCTION movies_vw_tr_func()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
   BEGIN
      IF TG_OP = 'INSERT' THEN
        INSERT INTO  movie_showing_table(movie_id, hall_id) VALUES(NEW.movie_id,NEW.hall_id);
        INSERT INTO  movies_table(movie_name, movie_duration,movie_rating) 
			VALUES(NEW.movie_name, NEW.movie_duration, NEW.movie_rating);
        RETURN NEW;
      ELSIF TG_OP = 'UPDATE' THEN
        UPDATE movie_showing_table
			SET movie_id = NEW.movie_id, hall_id = NEW.hall_id 
				WHERE movie_showing_id = OLD.movie_showing_id;
        UPDATE movies_table
			SET movie_name = NEW.movie_name,movie_duration = NEW.movie_duration, movie_rating = NEW.movie_rating 
				WHERE movie_id = OLD.movie_id;
		RETURN NEW;
      ELSIF TG_OP = 'DELETE' THEN
		DELETE FROM movies_table WHERE movie_id = OLD.movie_id;
		DELETE FROM movie_showing_table WHERE movie_id = OLD.movie_id;
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$function$;

CREATE TRIGGER movies_vw_tr
    INSTEAD OF INSERT OR UPDATE OR DELETE ON
      movies_vw FOR EACH ROW EXECUTE PROCEDURE movies_vw_tr_func();


