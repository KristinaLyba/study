__author__ = 'kris'
import random

#
#   movie_rating numeric NOT NULL DEFAULT 1,
#   PRIMARY KEY (movie_id),
#   FOREIGN KEY (movie_genre)
#       REFERENCES movie_genres (movie_genre_id)
# );
#
# CREATE TABLE movie_showing_table
# (
#   movie_showing_id serial,
#   hall_id integer NOT NULL,
#   movie_id integer NOT NULL,
#   showing_dates date NOT NULL DEFAULT CURRENT_DATE,
#   days_showing integer NOT NULL DEFAULT 7,
#   PRIMARY KEY (movie_showing_id),
#   FOREIGN KEY (movie_id)
#       REFERENCES movies_table (movie_id),
#   FOREIGN KEY (hall_id)
#       REFERENCES halls_table (hall_id)
#
# );

movie_durations = ["'01:54:00'", "'01:45:00'", "'02:14:00'", "'02:20:00'", "'01:30:00'", "'02:00:00'", "'01:05:00'"]
movie_rating = [1, 2, 3, 4, 5]
movie_id = "DEFAULT"
hall_ids = [1, 2, 3]
showing_dates = ["'2013-03-27'", "'2013-03-28'", "'2013-03-29'", "'2013-03-30'", "'2013-03-31'", "'2013-04-02'", \
                 "'2013-04-03'"]

f1 = open('requests_lab8_1.txt', 'w')
f2 = open('requests_lab8_2.txt', 'w')

#first movie_table
for i in range(1,10000):
    movie_name = "'movie"
    movie_duration = str(random.choice(movie_durations))
    rating = int(random.choice(movie_rating))
    movie_name += str(i) + "'"
    if i in range(1,1000):
        movie_genre = 1
    elif i in range(2000, 3000):
        movie_genre = 2
    elif i in range(3000, 4000):
        movie_genre = 3
    elif i in range(4000, 7000):
        movie_genre = 4
    elif i in range(7000, 8000):
        movie_genre = 5
    elif i in range(8000, 10000):
        movie_genre = 6

    request1 = 'INSERT INTO movies_table VALUES( DEFAULT, ' + movie_name + ', ' + movie_duration + ', '\
        + str(movie_genre) + ', ' + str(rating) + ');\n'

    #if i%2 == 1:
    hall_id = random.choice(hall_ids)
    showing_date = random.choice(showing_dates)
    days_showing = random.choice(range(7, 21))
    request2 = 'INSERT INTO movie_showing_table VALUES( DEFAULT, ' + str(hall_id) + ', ' + str(i) + ', '\
       + showing_date + ', ' + str(days_showing) + ');\n'
    f2.write(request2)
    f1.write(request1)


#second - movie_showing_table

print "Done"
f1.close()
f2.close()
