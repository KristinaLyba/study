hold off;
t = 0:0.01:1;
T = 1;
b = -0.5;
tau = 0.8*T;
y = (cos(2*t.*pi ) +b ).*(t-floor(t)<tau);
y = y + 0*((t-floor(t))>tau ).*((t-floor(t))< T);
subplot(3,1,1);plot(t,y,'r');
hold on;
title('z=function 8');
syms t k;
a0 = int(cos(2*t.*pi) +b ,t,0,tau)/2;
ak = int((cos(2*t.*pi) + b )*cos(2*pi*k*t),t,0,tau);
bk = int((cos(2*t.*pi) + b )*sin(2*pi*k*t),t,0,tau);
t2=0:0.01:1;
clear gar; 
y2 = a0;
for c = 1:50
    gar(c,:) = gara(c) * cos(2*pi*c*t2)+garb(c)* sin(2*pi*c*t2);
    y2 = y2 + gar(c,:);
end
%y2 = a0 + gar(1,:)+gar(2,:)+gar(3,:)+ gar(4,:)+gar(5,:);
subplot(3,1,1);plot(t2,2*y2,'g');

for k=1:10
    A(k)=sqrt(gara(k).^2 + garb(k).^2);
end
subplot(3,1,2);stem(A);

for k=1:10
   Ph(k) = atan2(garb(k),gara(k));
end
subplot(3,1,3);stem(Ph);
y2 = a0;
for c = 1:10
    gar(c,:) = A(c) * cos(2*pi*c*t2-Ph(c));
    y2 = y2 + gar(c,:);
end
subplot(3,1,1);plot(t2,y2*2,'b');

