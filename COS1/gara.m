function a1=gara(k)
    if k<=1
        a1=(64*pi - 5*2^(1/2)*(5 - 5^(1/2))^(1/2))/(160*pi) - sin((8*pi*k)/5)./(4*pi*k);
    else 
        a1 = sin((2*pi*(4*k + 1))/5)./(4*pi*(k - 1)) - cos((pi*(16*k + 1))/10)./(4*pi*(k + 1)) - sin((8*pi*k)/5)./(4*pi*k);
    end
 end
