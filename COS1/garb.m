function b1=garb(k)
    if k == 1
        b1 = (3*5^(1/2) - 5)/(32*pi);
    else 
        b1 =  - sin((4*pi*k)/5)^2/(2*pi*k) - (cos((2*pi*(4*k + 1))/5) - 2*k - sin((pi*(16*k + 1))/10) + k*cos((2*pi*(4*k + 1))/5) + k*sin((pi*(16*k + 1))/10))/(4*pi*(k^2 - 1));
    end 
end